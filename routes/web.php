<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'roles:admin|moderator'], function () {

    Route::get('/', function () {
        return view('admin.main');
    });
    Route::resource('drivers', 'DriverController')->only([
        'index'
    ]);
    Route::resource('passengers', 'PassengerController')->only([
        'index'
    ]);
    Route::resource('orders', 'OrderController')->only([
        'index', 'create', 'store'
    ]);

    Route::group(['middleware' => 'admin'], function () {

        Route::resource('drivers', 'DriverController')->except([
            'index', 'show'
        ]);
        Route::resource('passengers', 'PassengerController')->except([
            'index', 'show'
        ]);
        Route::resource('orders', 'OrderController')->except([
            'index', 'show', 'create', 'store'
        ]);
        Route::get('drivers/{id}/change', 'DriverController@change')->name('drivers.change');
        Route::get('passengers/{id}/change', 'PassengerController@change')->name('passengers.change');
    });
});

Route::group(['namespace' => 'Auth'], function () {

    Route::group(['middleware' => 'guest'], function () {
        Route::get('login', 'AuthController@loginForm')->name('login');
        Route::post('login', 'AuthController@login');
    });

    Route::post('logout', 'AuthController@logout')->middleware('auth');
});


