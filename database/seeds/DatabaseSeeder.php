<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
            'name' => rand(),
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'admin'
        ],
        [
            'name' => rand(),
            'email' => 'moderator@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'moderator'
        ]
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
