<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Driver;
use App\Models\Admin\Passenger;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PassengerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passengers = Passenger::all();

        return view('admin.passengers.index', ['passengers' => $passengers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.passengers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'patronymic' => 'required',
            'nickname' => 'required',
            'city' => 'required',
            'phone_num' => 'required|max:12',
            'email' => 'required|email|unique:passengers',
            'password' => 'required|min:8',
        ]);
        if($request->driver) {
            Driver::destroy($request->driver);
        }
        Passenger::add($request->all());
        return redirect()->route('passengers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $passenger = Passenger::find($id);
        return $passenger;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $passenger = Passenger::find($id);
        return view('admin.passengers.edit', ['passenger' => $passenger]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $passenger = Passenger::find($id);
        $this->validate($request, [
            'phone_num' => 'max:12',
            'email' => [
                'email',
                Rule::unique('passengers')->ignore($passenger->id)
            ],
            'password' => 'nullable|min:8',
        ]);
        $passenger->edit($request->all());
        return redirect()->route('passengers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Passenger::destroy($id);
        return redirect()->route('passengers.index');
    }

    public function change($id)
    {
        $passenger = Passenger::find($id);
        return view('admin.drivers.create', ['passenger' => $passenger]);
    }
}
