<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if (preg_match('/\|/', $roles))
        {
            $roles = explode('|', $roles);
        }
        else
        {
            $roles = [];
            $roles[] = $roles;
        }

        foreach ($roles as $role)
        {
            try
            {
                if (Auth::check() && Auth::user()->role == $role)
                {
                    return $next($request);
                }
            }
            catch (ModelNotFoundException $exception)
            {
                dd('Could not find role ' . $role);
            }
        }
        return redirect()->back()->with(['message' => 'У вас недостаточно прав для просмотра этой страницы']);
    }
}
