<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{

    protected $fillable = [
            'name', 'surname', 'patronymic', 'nickname', 'city', 'phone_num', 'car_brand', 'car_num'
    ];

    public function orders()
    {
        $this->hasMany(Order::class);
    }
}
