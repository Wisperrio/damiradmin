<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{

    protected $fillable = [
        'name', 'surname', 'patronymic', 'nickname', 'city', 'phone_num', 'email'
    ];

    public function orders()
    {
        $this->hasMany(Order::class);
    }

    public static function add($fields)
    {
        $passenger = new static;
        $passenger->fill($fields);
        $passenger->password = bcrypt($fields['password']);
        $passenger->save();
        return $passenger;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        if($fields['password']) {
            $this->password = bcrypt($fields['password']);
        }
        $this->save();
    }
}
