@extends('adminlte::page')

@section('title', 'AdminPanel')

@section('layout_boxed', 'false')

@section('content_header')
    <h1>Добавить заказ</h1>
    @include('admin.errors')
@stop

@section('content')
    {!! Form::open(['route' => 'orders.store']) !!}
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Куда</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="from" value="{{old('from')}}">
                <label for="exampleInputEmail1">Откуда</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="to" value="{{old('to')}}">
                <label for="exampleInputEmail1">Стоимость</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="cost" value="{{old('cost')}}">
                <label for="exampleInputEmail1">Комментарий</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="description" value="{{old('description')}}">
                <label for="exampleInputEmail1">ID Пассажира</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="passenger_id" value="{{old('passenger_id')}}">
                <label for="exampleInputEmail1">ID Водителя</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="driver_id" value="{{old('driver_id')}}">
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button class="btn btn-default"><a  href="{{route('orders.index')}}">Назад</a></button>
        <button class="btn btn-success pull-right">Добавить</button>
    </div>
    <!-- /.box-footer-->
    {!! Form::close() !!}
@stop