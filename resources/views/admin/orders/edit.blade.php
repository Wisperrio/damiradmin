@extends('adminlte::page')

@section('title', 'AdminPanel')

@section('layout_boxed', 'false')

@section('content_header')
    <h1>Изменить заказ</h1>
    @include('admin.errors')
@stop

@section('content')
    {!! Form::open(['route' => ['orders.update', $order->id], 'method'=>'put']) !!}
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Откуда</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" value="{{$order->from}}">
                <label for="exampleInputEmail1">Куда</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="surname" value="{{$order->to}}">
                <label for="exampleInputEmail1">Стоимость</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="patronymic" value="{{$order->cost}}">
                <label for="exampleInputEmail1">Комментарий</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="nickname" value="{{$order->description}}">
                <label for="exampleInputEmail1">ID Пассажира</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="city" value="{{$order->passenger_id}}">
                <label for="exampleInputEmail1">ID Водителя</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="phone_num" value="{{$order->driver_id}}">
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button class="btn btn-default"><a  href="{{route('orders.index')}}">Назад</a></button>
        <button class="btn btn-success pull-right">Изменить</button>
    </div>
    <!-- /.box-footer-->
    {!! Form::close() !!}
@stop