@extends('adminlte::page')

@section('title', 'AdminPanel')
@section('layout_boxed', 'false')

@section('content_header')
    <h1>Заказы</h1>
    <a href="{{route('orders.create')}}" class="btn btn-success">Добавить</a>
@stop

@section('content')
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Откуда</th>
            <th>Куда</th>
            <th>Стоимость</th>
            <th>Комментарий</th>
            <th>ID Пассажира</th>
            <th>ID Водителя</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>{{$order->id}}</td>
                <td>{{$order->from}}</td>
                <td>{{$order->to}}</td>
                <td>{{$order->cost}}</td>
                <td>{{$order->description}}</td>
                <td>{{$order->passenger_id}}</td>
                <td>{{$order->driver_id}}</td>
                <td><a href="{{route('orders.edit', $order->id)}}" class="fa fa-pencil">Изменить</a>

                    {{Form::open(['route'=>['orders.destroy', $order->id], 'method'=>'delete'])}}
                    <button onclick="return confirm('are you sure?')" type="submit" class="delete">
                        <i class="fa fa-remove">Удалить</i>
                    </button>

                    {{Form::close()}}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop