@extends('adminlte::page')

@section('title', 'AdminPanel')

@section('layout_boxed', 'false')

@section('content_header')
    <h1>Изменить пассажира</h1>
    @include('admin.errors')
@stop

@section('content')
    {!! Form::open(['route' => ['passengers.update', $passenger->id], 'method'=>'put']) !!}
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Имя</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" value="{{$passenger->name}}">
                <label for="exampleInputEmail1">Фамилия</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="surname" value="{{$passenger->surname}}">
                <label for="exampleInputEmail1">Отчество</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="patronymic" value="{{$passenger->patronymic}}">
                <label for="exampleInputEmail1">Никнейм</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="nickname" value="{{$passenger->nickname}}">
                <label for="exampleInputEmail1">Город</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="city" value="{{$passenger->city}}">
                <label for="exampleInputEmail1">Номер телефона</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="phone_num" value="{{$passenger->phone_num}}">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="email" value="{{$passenger->email}}">
                <label for="exampleInputEmail1">Пароль</label>
                <input type="password" class="form-control" id="exampleInputEmail1" placeholder="" name="password">
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button class="btn btn-default"><a  href="{{route('passengers.index')}}">Назад</a></button>
        <button class="btn btn-success pull-right">Изменить</button>
    </div>
    <!-- /.box-footer-->
    {!! Form::close() !!}
@stop