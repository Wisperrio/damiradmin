@extends('adminlte::page')

@section('title', 'AdminPanel')

@section('layout_boxed', 'false')

@section('content_header')
    <h1>Добавить пассажира</h1>
    @include('admin.errors')
@stop

@section('content')
        {!! Form::open(['route' => 'passengers.store']) !!}
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                    <label for="exampleInputEmail1">Имя</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" value="{{$driver->name ?? old('name')}}">
                    <label for="exampleInputEmail1">Фамилия</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="surname" value="{{$driver->surname ?? old('surname')}}">
                    <label for="exampleInputEmail1">Отчество</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="patronymic" value="{{$driver->patronymic ?? old('patronymic')}}">
                    <label for="exampleInputEmail1">Никнейм</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="nickname" value="{{$driver->nickname ?? old('nickname')}}">
                    <label for="exampleInputEmail1">Город</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="city" value="{{$driver->city ?? old('city')}}">
                    <label for="exampleInputEmail1">Номер телефона</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="phone_num" value="{{$driver->phone_num ?? old('phone_num')}}">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="email" value="{{$driver->email ?? old('email')}}">
                    <label for="exampleInputEmail1">Пароль</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" placeholder="" name="password">
                @if($driver)
                    <input type="hidden" name="driver" value="{{$driver->id}}">
                @endif
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button class="btn btn-success pull-right">Добавить</button>
    </div>
    <!-- /.box-footer-->
    {!! Form::close() !!}
    <div class="box-footer">
        <button class="btn btn-default"><a  href="{{route('passengers.index')}}">Назад</a></button>
    </div>
@stop