@extends('adminlte::page')

@section('title', 'AdminPanel')
@section('layout_boxed', 'false')

@section('content_header')
    <h1>Пассажиры</h1>
    <a href="{{route('passengers.create')}}" class="btn btn-success">Добавить</a>
@stop

@section('content')
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Отчество</th>
            <th>Никнейм</th>
            <th>Город</th>
            <th>Номер телефона</th>
            <th>Email</th>
            <th>Пароль</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($passengers as $passenger)
            <tr>
                <td>{{$passenger->id}}</td>
                <td>{{$passenger->name}}</td>
                <td>{{$passenger->surname}}</td>
                <td>{{$passenger->patronymic}}</td>
                <td>{{$passenger->nickname}}</td>
                <td>{{$passenger->city}}</td>
                <td>{{$passenger->phone_num}}</td>
                <td>{{$passenger->email}}</td>
                <td>{{$passenger->password}}</td>
                <td><a href="{{route('passengers.edit', $passenger->id)}}" class="fa fa-pencil">Изменить</a><br>
                <a href="{{route('passengers.change', $passenger->id)}}" class="fa fa-pencil">Стать пассажиром</a>

                    {{Form::open(['route'=>['passengers.destroy', $passenger->id], 'method'=>'delete'])}}
                    <button onclick="return confirm('are you sure?')" type="submit" class="delete">
                        <i class="fa fa-remove">Удалить</i>
                    </button>

                    {{Form::close()}}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop