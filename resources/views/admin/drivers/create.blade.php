@extends('adminlte::page')

@section('title', 'AdminPanel')

@section('layout_boxed', 'false')

@section('content_header')
    <h1>Добавить водителя</h1>
    @include('admin.errors')
@stop

@section('content')
    {!! Form::open(['route' => 'drivers.store']) !!}
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Имя</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" value="{{$passenger->name ?? old('name')}}">
                <label for="exampleInputEmail1">Фамилия</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="surname" value="{{$passenger->surname ?? old('surname')}}">
                <label for="exampleInputEmail1">Отчество</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="patronymic" value="{{$passenger->patronymic ?? old('patronymic')}}">
                <label for="exampleInputEmail1">Никнейм</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="nickname" value="{{$passenger->nickname ?? old('nickname')}}">
                <label for="exampleInputEmail1">Город</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="city" value="{{$passenger->city ?? old('city')}}">
                <label for="exampleInputEmail1">Номер телефона</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="phone_num" value="{{$passenger->phone_num ?? old('phone_num')}}">
                <label for="exampleInputEmail1">Марка авто</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="car_brand" value="{{$passenger->car_brand ?? old('car_brand')}}">
                <label for="exampleInputEmail1">Номер авто</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="car_num" value="{{$passenger->car_num ?? old('car_num')}}">
                @if($passenger)
                    <input type="hidden" name="passenger" value="{{$passenger->id}}">
                @endif
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button class="btn btn-default"><a  href="{{route('drivers.index')}}">Назад</a></button>
        <button class="btn btn-success pull-right">Добавить</button>
    </div>
    <!-- /.box-footer-->
    {!! Form::close() !!}
@stop