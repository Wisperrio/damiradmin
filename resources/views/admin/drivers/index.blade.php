@extends('adminlte::page')

@section('title', 'AdminPanel')
@section('layout_boxed', 'false')

@section('content_header')
    <h1>Водители</h1>
    <a href="{{route('drivers.create')}}" class="btn btn-success">Добавить</a>
@stop

@section('content')
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Отчество</th>
            <th>Никнейм</th>
            <th>Город</th>
            <th>Номер телефона</th>
            <th>Марка авто</th>
            <th>Номер авто</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($drivers as $driver)
            <tr>
                <td>{{$driver->id}}</td>
                <td>{{$driver->name}}</td>
                <td>{{$driver->surname}}</td>
                <td>{{$driver->patronymic}}</td>
                <td>{{$driver->nickname}}</td>
                <td>{{$driver->city}}</td>
                <td>{{$driver->phone_num}}</td>
                <td>{{$driver->car_brand}}</td>
                <td>{{$driver->car_num}}</td>
                <td><a href="{{route('drivers.edit', $driver->id)}}" class="fa fa-pencil">Изменить</a><br>
                <a href="{{route('drivers.change', $driver->id)}}" class="fa fa-pencil">Стать пассажиром</a>

                    {{Form::open(['route'=>['drivers.destroy', $driver->id], 'method'=>'delete'])}}
                    <button onclick="return confirm('are you sure?')" type="submit" class="delete">
                        <i class="fa fa-remove">Удалить</i>
                    </button>

                    {{Form::close()}}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop