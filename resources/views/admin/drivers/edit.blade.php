@extends('adminlte::page')

@section('title', 'AdminPanel')

@section('layout_boxed', 'false')

@section('content_header')
    <h1>Изменить водителя</h1>
    @include('admin.errors')
@stop

@section('content')
    {!! Form::open(['route' => ['drivers.update', $driver->id], 'method'=>'put']) !!}
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Имя</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" value="{{$driver->name}}">
                <label for="exampleInputEmail1">Фамилия</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="surname" value="{{$driver->surname}}">
                <label for="exampleInputEmail1">Отчество</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="patronymic" value="{{$driver->patronymic}}">
                <label for="exampleInputEmail1">Никнейм</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="nickname" value="{{$driver->nickname}}">
                <label for="exampleInputEmail1">Город</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="city" value="{{$driver->city}}">
                <label for="exampleInputEmail1">Номер телефона</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="phone_num" value="{{$driver->phone_num}}">
                <label for="exampleInputEmail1">Марка авто</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="car_brand" value="{{$driver->car_brand}}">
                <label for="exampleInputEmail1">Номер авто</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="car_num" value="{{$driver->car_num}}">
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <button class="btn btn-default"><a  href="{{route('drivers.index')}}">Назад</a></button>
        <button class="btn btn-success pull-right">Изменить</button>
    </div>
    <!-- /.box-footer-->
    {!! Form::close() !!}
@stop